package Gojek.UiPackage;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class CreditCardPage {
	


	
	@FindBy(how=How.XPATH,using="//input[@name=\"cardnumber\"]")
	@CacheLookup
	public static WebElement CardNumber;
	
	@FindBy(how=How.XPATH,using="//input[@placeholder=\"MM / YY\"]")
	@CacheLookup
	public static WebElement ExpireDate;
	
	@FindBy(how=How.XPATH,using="//*[@id=\"application\"]/div[3]/div/div/div/form/div[2]/div[3]/input")
	@CacheLookup
	public static WebElement CVV;
	
	@FindBy(how=How.XPATH,using="//a[@class='button-main-content']")
	@CacheLookup
	public static WebElement PayNow;
	
	
	
	

}
